import 'package:flutter/material.dart';

class Account extends StatelessWidget {

  final topBar = new AppBar(
    backgroundColor: new Color(0xfff8faf8),
    centerTitle: true,
    elevation: 1.0,
    leading: new Icon(Icons.camera_alt),
    title: SizedBox(
      height: 35.0,
      child: Text('Rasyiidgram'),
    ),
    actions: <Widget>[
      Padding(
        padding: const EdgeInsets.only(right: 12.0),
        child: Icon(Icons.send),
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: topBar,
      body: Center(child: Text('Account'),),
      bottomNavigationBar : new Container(
        color: Colors.white,
        height: 50.0,
        child: new BottomAppBar(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  Navigator.of(context).pushNamed('/');
                },
              ),
              new IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  Navigator.of(context).pushNamed('/explore');
                },
              ),
              new IconButton(
                icon: Icon(Icons.add_box),
                onPressed: () {
                  Navigator.of(context).pushNamed('/add');
                },
              ),
              new IconButton(
                icon: Icon(Icons.favorite),
                onPressed: () {
                  Navigator.of(context).pushNamed('/favorite');
                },
              ),
              new IconButton(
                icon: Icon(Icons.account_box),
                onPressed: () {
                  Navigator.of(context).pushNamed('/account');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}