import 'package:Rasyiidgram/insta_add.dart';
import 'package:Rasyiidgram/insta_explore.dart';
import 'package:flutter/material.dart';
import 'package:Rasyiidgram/insta_home.dart';
import 'package:Rasyiidgram/insta_favorite.dart';
import 'package:Rasyiidgram/insta_account.dart';
import 'package:Rasyiidgram/insta_stories.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings){
    final args = settings.arguments;

    switch(settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => instaHome());
      case '/explore':
        return MaterialPageRoute(builder: (_) => Explore());
      case '/add':
        return MaterialPageRoute(builder: (_) => Add());
      case '/favorite':
        return MaterialPageRoute(builder: (_) => Favorite());
      case '/account':
        return MaterialPageRoute(builder: (_) => Account());
      case '/story':
        return MaterialPageRoute(builder: (_) => story());
      default:
        return MaterialPageRoute(builder: (_) => instaHome());
    }
  }
}